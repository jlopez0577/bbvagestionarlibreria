Useful information
------------------

1. Start application: mvn clean appengine:devserver

2. Access application: http://localhost:8080

3. Access swagger: http://localhost:8080/swagger/



5. Access Google console: https://console.developers.google.com

6. Deploy in Google: mvn clean appengine:update


Documentation
-------------

Google App Engine Java: https://cloud.google.com/appengine/docs/java/

Objectify: https://github.com/objectify/objectify

AngularJS: https://angularjs.org/



REPOSITORIO BITBUCKET
---------------------

https://bitbucket.org/jlopez0577/bbvagestionarlibreria.git

SERVICIOS REST POSTMAN (YA DESPLEGADO GOOGLE)
----------------------------------------------
1. http://bbvagestionarlibreria-176507.appspot.com/rest/libros/get/all
2. http://bbvagestionarlibreria-176507.appspot.com/rest/libros/add	
3. http://bbvagestionarlibreria-176507.appspot.com/rest/libros/get/{id}
4. http://bbvagestionarlibreria-176507.appspot.com/rest/libros/delete/{id}

json para el rest /add --> 	{"id": 3,"nombre": "Ali-Baba","autor": "Desconocido","anyoPublicacion": 2000,"genero": "infantil"}

Documentation
-------------

Google App Engine Java: https://cloud.google.com/appengine/docs/java/

Objectify: https://github.com/objectify/objectify

DESPLEGAR CONSOLE GOOGLE
------------------------

TUTORIALDIR=~/src/bbvagestionarlibreria-176507/java_gae_quickstart-2017-08-11-09-37

1º vez --> git clone https://bitbucket.org/jlopez0577/bbvagestionarlibreria.git $TUTORIALDIR

las otras vecez -->git pull https://bitbucket.org/jlopez0577/bbvagestionarlibreria.git $TUTORIALDIR

mvn appengine:deploy

