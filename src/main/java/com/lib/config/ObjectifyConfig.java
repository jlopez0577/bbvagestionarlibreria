package com.lib.config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.impl.translate.opt.joda.DateTimeZoneTranslatorFactory;
import com.googlecode.objectify.impl.translate.opt.joda.LocalDateTimeTranslatorFactory;
import com.googlecode.objectify.impl.translate.opt.joda.LocalDateTranslatorFactory;
import com.googlecode.objectify.impl.translate.opt.joda.ReadableInstantTranslatorFactory;
import com.lib.data.LibrosBean;


/**
 * Class to load bean to persistent into DataStore.
 */
public class ObjectifyConfig implements ServletContextListener{
	
	
	static {
        // Add translators for Joda objects

        ObjectifyService.factory().getTranslators()
                .add(new ReadableInstantTranslatorFactory());
        ObjectifyService.factory().getTranslators()
                .add(new LocalDateTranslatorFactory());
        ObjectifyService.factory().getTranslators()
                .add(new LocalDateTimeTranslatorFactory());
        ObjectifyService.factory().getTranslators()
                .add(new DateTimeZoneTranslatorFactory());

        // TODO: Add and register each data bean here
        ObjectifyService.register(LibrosBean.class);
    }


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
