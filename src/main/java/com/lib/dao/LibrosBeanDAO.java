package com.lib.dao;

import java.util.List;
import java.util.logging.Logger;

import com.googlecode.objectify.ObjectifyService;
import com.lib.data.LibrosBean;


public class LibrosBeanDAO {

    private static final Logger LOGGER = Logger.getLogger(LibrosBeanDAO.class.getName());

    /**
     * @return Devuelve una lista de libros.
     */
    public List<LibrosBean> list() {
        LOGGER.info("recibo una lista de libros");
        return ObjectifyService.ofy().load().type(LibrosBean.class).list();
    }
    
    /**
     * @return Lista de libros ordenada por autor.
     */
    public List<LibrosBean> listPorAutor() {
        LOGGER.info("Recuperando libros ordenados por autor");
        List<LibrosBean> retorno = ObjectifyService.ofy().load().type(LibrosBean.class).order("autor").list();
        LOGGER.info("Recuperados " +retorno.size()+" libros ");
        return retorno;
    }


    /**
     * @param id
     * @return devuelve un libro a partir de su id.
     */
    public LibrosBean get(Long id) {
        LOGGER.info("Recibido id: " + id);
        return ObjectifyService.ofy().load().type(LibrosBean.class).id(id).now();
    }

    /**
     * Añade un LibrosBean
     * @param bean
     */
    public void add(LibrosBean bean) {
        if (bean == null) {
            throw new IllegalArgumentException("null LibrosBean object");
        }
        LOGGER.info("Se va añadir el libro con id: " + bean.getId()+", y nombre: "+bean.getNombre());
        ObjectifyService.ofy().save().entity(bean).now();
        LOGGER.info("Libro añadido");
    }

    /**
     * Borra el Libro dado
     * @param bean
     */
    public void delete(LibrosBean bean) {
        if (bean == null) {
            throw new IllegalArgumentException("null LibrosBean object");
        }
        LOGGER.info("Se va a borrar el libro con id: " + bean.getId()+", y nombre: "+bean.getNombre());
        ObjectifyService.ofy().delete().entity(bean);
        LOGGER.info("Libro borrado");
    }
}
