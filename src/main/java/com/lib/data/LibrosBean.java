package com.lib.data;

import com.googlecode.objectify.annotation.Cache;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;
import com.wordnik.swagger.annotations.ApiModel;


@Entity
@Cache
@ApiModel("objeto libro")
public class LibrosBean {
	
	
	 @Id
	 private Long id;
	 
	 private String nombre;
	 @Index
	 private String autor;
	 private Integer anyoPublicacion;
	 private String genero;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public Integer getAnyoPublicacion() {
		return anyoPublicacion;
	}
	public void setAnyoPublicacion(Integer anyoPublicacion) {
		this.anyoPublicacion = anyoPublicacion;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}

	 
	 
}
