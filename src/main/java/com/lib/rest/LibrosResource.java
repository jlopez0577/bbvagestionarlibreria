package com.lib.rest;

import com.lib.dao.LibrosBeanDAO;
import com.lib.data.LibrosBean;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("libros")
@Produces("application/json;charset=utf-8")
@Api(value = "libros", description = "Servicio libros")
public class LibrosResource {
	
	private LibrosBeanDAO librosBeanDAO;

	public LibrosResource() {
        this.librosBeanDAO = new LibrosBeanDAO();
    }

    @GET
    @Path("/get/all")
    @ApiOperation("list objetos libro")
    public Response list() {
        return Response.ok(this.librosBeanDAO.listPorAutor()).build();
    }

    @GET
    @Path("/get/{id}")
    @ApiOperation("get objeto libro por id")
    public Response get(@PathParam("id") Long id) {
        LibrosBean bean = this.librosBeanDAO.get(id);
        if (bean == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(bean).build();
    }

    @POST
    @Path("/add")
    @Consumes("application/json;charset=utf-8")
    @ApiOperation("save objeto libro")
    public Response save(LibrosBean bean) {
        this.librosBeanDAO.add(bean);
        return Response.ok().build();
    }

    @DELETE
    @Path("/delete/{id}")
    @ApiOperation("delete objeto libro")
    public Response delete(@PathParam("id") Long id) {
        LibrosBean bean = this.librosBeanDAO.get(id);
        if (bean == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        this.librosBeanDAO.delete(bean);
        return Response.ok().build();
    }

}
