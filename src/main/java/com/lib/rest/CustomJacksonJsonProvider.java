package com.lib.rest;

import java.io.IOException;

import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.module.SimpleModule;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

//import com.fasterxml.jackson.annotation.JsonInclude.Include;
//import com.fasterxml.jackson.core.JsonGenerator;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonSerializer;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializerProvider;
//import com.fasterxml.jackson.databind.module.SimpleModule;
//import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;



@Provider
public class CustomJacksonJsonProvider extends JacksonJsonProvider{

	public CustomJacksonJsonProvider() {
		super();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
        mapper.setSerializationInclusion(Inclusion.NON_NULL);
        SimpleModule module = new SimpleModule("JsonProvider",new Version(1,0,0,""));
        module.addSerializer(DateTime.class, new JsonSerializer<DateTime>() {
            @Override
            public void serialize(DateTime dt, JsonGenerator generator,
                    SerializerProvider provider) throws IOException,
                    JsonProcessingException {
                if (dt != null) {
                    generator.writeString(ISODateTimeFormat
                            .dateHourMinuteSecond().print(dt));
                } else {
                    generator.writeNull();
                }
            }
        });
        mapper.registerModule(module);
        setMapper(mapper);
	}


	

}
